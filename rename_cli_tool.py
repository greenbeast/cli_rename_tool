import argparse
from shutil import move

try:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o",
        action="store",
        dest="original",
        help="Type the filename that you want to change. Only works with files without spaces.",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-n",
        action="store",
        dest="new",
        help="Type the filename you want the file to be.",
        type=str,
        required=True,
    )
    args = parser.parse_args()

    old = args.original
    new = args.new

    name = move(old, new)
    print(f"{old} has been renamed to {name}")
except:
    print(
        "This didn't work because you chose a filename that isn't in this directory or your path is incorrect."
    )
